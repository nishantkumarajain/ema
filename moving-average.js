const _ = require('lodash');

/**
 * Calculates the sum of prices
 * @param {*} stockDetails
 */
const sum = stockDetails => 
  stockDetails.reduce((sum, stockDetail) => sum + stockDetail.closePrice, 0);

/**
 * Calculates the average of prices.
 * @param {*} stockDetails 
 */
const average = (stockDetails) => {
  const value = sum(stockDetails) / stockDetails.length;
  // return +(Math.round( value * 100 ) / 100).toFixed(2);
  return value;
}; 

/**
 * Creates a window function based on the 
 * window length.
 * @param {*} length 
 */
const makeWindow = length => (_number, index, array) => {
  if (index >= length - 1) {
    const startIndex = Math.max(0, (index - length) + 1);
    const endIndex = index + 1;
    return array.slice(startIndex, endIndex);
  }
  return undefined;
};

/**
 * 
 * @param {*} previousEMAValues 
 * @param {*} currentEMAValues 
 */
const detectCrossover = (previousEMAValues, currentEMAValues) => {
  if ((currentEMAValues[0].emaValue <= currentEMAValues[1].emaValue && previousEMAValues[0].emaValue >= previousEMAValues[1].emaValue) || 
  (currentEMAValues[0].emaValue >= currentEMAValues[1].emaValue && previousEMAValues[0].emaValue <= previousEMAValues[1].emaValue)) {
    return true;
  } else {
    return false;
  }
};

/**
 * 
 * @param {*} DataCollectionObject - Contains the previous and current stock details
 * @param {*} ParametersObject - Contains emaDays
 */
const smaCalculator = ({ stockDetails }, { emaDays }) => {
  const emaValues = emaDays.map((emaDay) => {
    const windowedSmaValues = stockDetails
      .map(makeWindow(emaDay))
    const emaValue = +average(windowedSmaValues[windowedSmaValues.length - 1]);
    return { emaDay, emaValue };
  });
  stockDetails[stockDetails.length - 1].emaValues = emaValues;
  return { stockDetails };
}

/**
 * Calculates the EMA for a current price.
 * @param {*} currentPrice - Refers to the current price
 * @param {*} prevEma - Refers to the prevEMA
 * @param {*} period - Refers to the period or the window
 */
const ema = (currentPrice, emaDay, prevEma) => {
  const smoothingConstant = 2 / (emaDay + 1);
  const value = (currentPrice * smoothingConstant) + (prevEma * (1 - smoothingConstant));
  // const emaValue = +(Math.round( value * 100 ) / 100).toFixed(2);
  return { emaDay, emaValue: value };
};

/**
 * Calculates the EMA Calculator
 * @param {*} DataCollectionObject - Contains the previous and current stock details
 * @param {*} ParametersObject - Contains emaDays
 */
const emaCalculator = ({ previousStockDetails, currentStockDetails }, { emaDays }) => {
  currentStockDetails.emaValues = emaDays.map((emaDay, index) =>
    ema(currentStockDetails.closePrice, emaDay, previousStockDetails.emaValues[index].emaValue));
  const hasCrossoverHappened = detectCrossover(previousStockDetails.emaValues, currentStockDetails.emaValues);
  return { previousStockDetails, currentStockDetails, hasCrossoverHappened };
};

module.exports = { smaCalculator, emaCalculator };