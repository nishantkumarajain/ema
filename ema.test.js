const { smaCalculator, emaCalculator } = require('./moving-average.js');
const should = require('should');

describe('Test EMA', () => {
  test('Should be able to calcuate the SMA', () => {
    const dataCollectionObject = {
      stockDetails: [
        { closePrice: 445.73, date: "7/22/2010" },
        { closePrice: 444.94, date: "7/23/2010" },
        { closePrice: 443.85, date: "7/24/2010" },
        { closePrice: 444.74, date: "7/27/2010" },
        { closePrice: 444.84, date: "7/28/2010" },
        { closePrice: 444.34, date: "7/29/2010" },
        { closePrice: 445.34, date: "7/30/2010" },
        { closePrice: 447.32, date: "8/3/2010" },
        { closePrice: 445.44, date: "8/4/2010" },
        { closePrice: 445.93, date: "8/5/2010" },
      ]
    };
    const parametersObject = {
      emaDays: [3, 10],
    };
    const expectedOutput = {
      stockDetails: [
        { closePrice: 445.73, date: "7/22/2010" },
        { closePrice: 444.94, date: "7/23/2010" },
        { closePrice: 443.85, date: "7/24/2010" },
        { closePrice: 444.74, date: "7/27/2010" },
        { closePrice: 444.84, date: "7/28/2010" },
        { closePrice: 444.34, date: "7/29/2010" },
        { closePrice: 445.34, date: "7/30/2010" },
        { closePrice: 447.32, date: "8/3/2010" },
        { closePrice: 445.44, date: "8/4/2010" },
        {
          closePrice: 445.93,
          date: "8/5/2010",
          emaValues: [
            {
              emaDay: 3,
              emaValue: 446.23
            },
            {
              emaDay: 10,
              emaValue: 445.247
            }
          ]
        },
      ],
    };
    const actualOutput = smaCalculator(dataCollectionObject, parametersObject);
    should.exist(actualOutput)
    expectedOutput.should.be.eql(actualOutput);
  });
  test('Should be able to calculate the EMA', () => {
    const dataCollectionObject = {
      previousStockDetails: {
        closePrice: 445.93,
        date: "8/5/2010",
        emaValues: [
          {
            emaDay: 3,
            emaValue: 446.23
          },
          {
            emaDay: 10,
            emaValue: 445.247
          }
        ]
      },
      currentStockDetails: {
        closePrice: 444.54,
        date: "8/6/2010",
        emaValues: null
      }
    };
    
    const parametersObject = {
      emaDays: [3, 10]
    };
    
    const expectedOutput = {
      previousStockDetails: {
        closePrice: 445.93,
        date: "8/5/2010",
        emaValues: [
          {
            emaDay: 3,
            emaValue: 446.23
          },
          {
            emaDay: 10,
            emaValue: 445.247
          }
        ]
      },
      currentStockDetails: {
        closePrice: 444.54,
        date: "8/6/2010",
        emaValues: [
          {
            emaDay: 3,
            emaValue: 445.385
          },
          {
            emaDay: 10,
            emaValue: 445.11845454545454
          }
        ]
      },
      hasCrossoverHappened: false
    };
    const actualOutput = emaCalculator(dataCollectionObject, parametersObject);
    should.exist(actualOutput);
    expectedOutput.should.eql(actualOutput);
  });
});