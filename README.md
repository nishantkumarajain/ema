# Exponential Moving Average

## Instructions

```
# This installs all the node dependencies
yarn/npm install

# This runs the test case on the the moving-average
yarn/npm run test

# This runs code coverage
yarn run test --coverage
```